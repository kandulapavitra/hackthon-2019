package com.hackathon;

import java.util.Stack;

public class ExpressionEvaluation {
	public static int evaluateExpression(String expression) {
		char[] expressionArray = expression.toCharArray();
		Stack<Integer> operandStack = new Stack<Integer>();
		Stack<Character> operatorStack = new Stack<Character>();
		for (int i = 0; i < expressionArray.length; i++) {
			if (expressionArray[i] >= '0' && expressionArray[i] <= '9') {
				StringBuffer num = new StringBuffer();
				int j = i;
				while (j < expressionArray.length && expressionArray[j] >= '0' && expressionArray[j] <= '9')
					num.append(expressionArray[j++]);
				operandStack.push(Integer.parseInt(num.toString()));
			} else if (expressionArray[i] == '(')
				operatorStack.push(expressionArray[i]);
			else if (expressionArray[i] == ')') {
				while (operatorStack.peek() != '(')
					operandStack.push(performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop()));
				operatorStack.pop();
			} else if (expressionArray[i] == '+' || expressionArray[i] == '-' || expressionArray[i] == '*'
					|| expressionArray[i] == '/') {
				while (!operatorStack.empty() && hasPrecedence(expressionArray[i], operatorStack.peek()))
					operandStack.push(performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop()));
				operatorStack.push(expressionArray[i]);
			}
		}
		while (!operatorStack.empty())
			operandStack.push(performOperation(operatorStack.pop(), operandStack.pop(), operandStack.pop()));
		return operandStack.pop();
	}

	public static boolean hasPrecedence(char op1, char op2) {
		if (op2 == '(' || op2 == ')')
			return false;
		if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
			return false;
		else
			return true;
	}

	public static int performOperation(char op, int b, int a) {
		switch (op) {
		case '+':
			System.out.println(a + " " + op + " " + b + " = " + (a + b));
			return a + b;
		case '-':
			System.out.println(a + " " + op + " " + b + " = " + (a - b));
			return a - b;
		case '*':
			System.out.println(a + " " + op + " " + b + " = " + (a * b));
			return a * b;
		case '/':
			if (b == 0)
				throw new UnsupportedOperationException("Cannot divide by zero");
			System.out.println(a + " " + op + " " + b + " = " + (a / b));
			return a / b;
		}
		return 0;
	}

	public static void main(String[] args) {
		ExpressionEvaluation expressionEvaluationObject = new ExpressionEvaluation();
		int result = expressionEvaluationObject.evaluateExpression("2 + 3 + ( 4 * 5 )");
		System.out.println("Final result:" + result);
	}
}